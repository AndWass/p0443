#pragma once

#include "../../util/detect.hpp"

#include <optional>
#include <utility>

#include "execute_impl.hpp"
#include "submit_impl.hpp"
#include "set_value_impl.hpp"
#include "set_done_impl.hpp"
#include "set_error_impl.hpp"
#include "schedule_impl.hpp"

#include "execute_impl.ipp"
#include "submit_impl.ipp"